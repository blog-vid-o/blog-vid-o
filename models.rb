# coding: utf-8

class Video < ActiveRecord::Base
	validates_presence_of :nom, :date, :fichier
	has_many :videotags, :dependent => :destroy
	has_many :tags, :through => :videotags
	
	after_create do
		if File.exist?("public/videos/#{self.fichier}")
			new_dir = "public/videos/#{self.id}"
			FileUtils.mkdir new_dir
			FileUtils.move "public/videos/#{self.fichier}", new_dir
			`ffmpeg -ss 00:20 -i "#{File.join(new_dir,self.fichier)}" "#{new_dir}/screenshot.jpg"`
		end
	end
	
	def add_tag tags
		unless tags.is_a?(Array)
			tags = [tags]
		end
		tags.each do |tag|
			t = Tag.where(nom: tag).first
			t = Tag.create(nom: tag) unless t
			vt = Videotag.where(tag_id: t.id, video_id: self.id).first
			unless vt
				Videotag.create(tag_id: t.id, video_id: self.id)
			end
		end
	end
	
	def annee
		year = self.date.year
		date = Date.parse("31/07/#{year}")
		if self.date<=date
			return "#{year-1}-#{year}"
		else
			return "#{year}-#{year+1}"
		end
	end
	
end

class Tag < ActiveRecord::Base
	validates_presence_of :nom
	validates_uniqueness_of :nom
	has_many :videotags, :dependent => :destroy
	has_many :videos, :through => :videotags
end

class Videotag < ActiveRecord::Base
	belongs_to :tag
	belongs_to :video
end
