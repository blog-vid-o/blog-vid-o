class CreateVideos < ActiveRecord::Migration
  def up
  	create_table :videos do |t|
  		t.string :nom, null: false
  		t.date :date, null: false
  		t.string :fichier, null: false
  		t.timestamps null: false
  	end
  	
  	create_table :tags do |t|
  		t.string :nom, null: false
  		t.timestamps null: false
  	end
  	
  	create_table :videotags do |t|
  		t.belongs_to :tag, index: true
  		t.belongs_to :video, index: true
  	end
  	
  end
  
  def down
  	drop_table :videos
  	drop_table :tags
  	drop_table :videotags
  end
end
