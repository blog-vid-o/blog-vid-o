# encoding: UTF-8

def prepare_videos vids
	videos = {}
	array_vid = []
	annee = vids[0].annee
	vids.each do |video|
		if annee!=video.annee
			videos.merge!(annee => array_vid)
			array_vid = []
			annee = video.annee
		end
		array_vid << video
	end
	videos.merge!(annee => array_vid) if array_vid.count>0
	
	return videos
end
