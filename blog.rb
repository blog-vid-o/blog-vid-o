#!/usr/bin/env ruby
# encoding: UTF-8

require 'bundler/setup'
require 'sinatra'
require 'yaml'
require 'haml'
require 'barista'
require 'bcrypt'
require 'sinatra/activerecord'
require 'fileutils'
require './helpers.rb'
require './models.rb'

configure do
  enable :sessions
  register Barista::Integration::Sinatra
end

get '/' do
	redirect to '/videos'
end

get '/videos' do
	@videos = prepare_videos Video.includes(:tags).order("date DESC")
  haml :videos
end

get '/videos/tag/:id' do
	@tag = Tag.find(params[:id].to_i)
	videotags = Videotag.where(:tag_id => params[:id].to_i)
	video_ids = []
	videotags.each do |vt|
		video_ids << vt.video_id
	end
	@videos = prepare_videos Video.includes(:tags).where(:id => video_ids).order("date DESC")
	haml :tag
end

get '/video/:id' do
	@previous = session[:previous_url] || '/'
	@video = nil
	begin
		@video = Video.find(params[:id].to_i)
	rescue
		redirect '/404'
	end
	haml :video
end

get '/video/:id/download' do
	video = nil
	begin
		video = Video.find(params[:id].to_i)
	rescue
	end
	if video
		fichier = "public/videos/#{video.id}/#{video.fichier}"
		send_file(fichier, :filename => video.fichier, :type => 'Application/octet-stream') if File.exist?(fichier)
	end
	where_user_came_from = session[:previous_url] || '/'
	redirect to where_user_came_from
end
